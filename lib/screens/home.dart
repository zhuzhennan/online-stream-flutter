import 'package:flutter/material.dart';
import '../utils/http.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import './login/index.dart';
import '../base.dart';

class Choice {
  const Choice({ this.title, this.icon });
  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: '全部', icon: Icons.directions_car),
  const Choice(title: '英雄联盟', icon: Icons.directions_bike),
  const Choice(title: 'DNF', icon: Icons.directions_boat),
  const Choice(title: '和平精英', icon: Icons.directions_bus),
  const Choice(title: '户外直播', icon: Icons.directions_railway),
  const Choice(title: '卡尔萨斯', icon: Icons.directions_walk),
  const Choice(title: '审判天使', icon: Icons.directions_walk),
  const Choice(title: '无极之道', icon: Icons.directions_walk),
  const Choice(title: '诺克萨斯', icon: Icons.directions_walk),
  const Choice(title: '一起看', icon: Icons.directions_walk),
];

class ChoiceCard extends StatelessWidget {
  const ChoiceCard({ Key key, this.choice }) : super(key: key);

  final Choice choice;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.display1;
    return new Card(
      color: Colors.white,
      child: new Center(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Icon(choice.icon, size: 128.0, color: textStyle.color),
            new Text(choice.title, style: textStyle),
          ],
        ),
      ),
    );
  }
}


class HomeScreen extends StatefulWidget {
  final bool gray;

  HomeScreen({this.gray = true});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

// with 多继承
class _HomeScreenState extends State<HomeScreen>with AutomaticKeepAliveClientMixin, DYBase {
  TextEditingController _search = TextEditingController();
  List _imgList = [];
  ///加载图片的标识
  bool isLoadingImage = true;
  RefreshController _controller = RefreshController(initialRefresh: false);

  Future _scan() async {
    print(DateTime.now());
    // try {
    //   _search.text = await BarcodeScanner.scan();
    // } on PlatformException catch (e) {
    //   if (e.code == BarcodeScanner.CameraAccessDenied) {
    //     DYdialog.alert(context, text: '设备未获得权限');
    //   } else {
    //     DYdialog.alert(context, text: '未捕获的错误: $e');
    //   }
    // } on FormatException {  // 用户手动点击设备返回
    //
    // } catch (e) {
    //   DYdialog.alert(context, text: '未捕获的错误: $e');
    // }
  }

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds: 1000));
    _controller.refreshCompleted();
  }

  void _onLoading() async{
    await Future.delayed(Duration(milliseconds: 1500));

    if(mounted)
      setState(() {

      });
    _controller.loadComplete();
  }

  Widget _itemBuilder(BuildContext context, int position) {
    return buildListView();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: choices.length,
      child: Scaffold(
        appBar: AppBar(
          leading:IconButton(icon: Icon(Icons.dehaze), onPressed: () {
            Scaffold.of(context).openDrawer();
          }),
          bottom: TabBar(
            isScrollable: true,
            tabs: choices.map((Choice choice) {
              return Tab(
                text: choice.title,
                // icon: Icon(choice.icon),
              );
            }).toList(),
            onTap: (int index) {
              print(index);
            },
          ),
          title: Container(
            height: 35,
            // margin: EdgeInsets.only(left: 15),
            // padding: EdgeInsets.only(left: 5, right: 5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(17.5),
              ),
            ),
            child: Row(
              children: <Widget>[
                // 搜索ICON
                Image.asset(
                  'images/head/search.webp',
                  width: 20,
                  height: 20,
                ),
                // 搜索输入框
                Expanded(
                  flex: 1,
                  child: TextField(
                    controller: _search,
                    cursorColor: DYBase.defaultColor,
                    cursorWidth: 1.5,
                    style: TextStyle(
                      color: DYBase.defaultColor,
                      fontSize: 14.0,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      // contentPadding: EdgeInsets.all(0),
                      hintText: '金咕咕doinb',
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: _scan,
                  child: Image.asset(
                    'images/head/camera.webp',
                    width: 20,
                    height: 15,
                  ),
                ),
              ],
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0,
          actions:<Widget>[
            IconButton(
                icon: Icon(Icons.access_time_outlined),
                onPressed: (){
                  Navigator.pushNamed(context, '/view-history', arguments: '路由参数');
                }
            ),
            GestureDetector(
              onTap: () {
                print('MyButton was tapped!');
              },
              child: Image.asset(
                widget.gray ? 'images/head/game-gray.webp' : 'images/head/game.webp',
                width: 25.0,
                height: 15,
              ),
            ),
            IconButton(
                icon:Icon(Icons.messenger_outline_sharp),
                onPressed: (){
                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => LoginScreen(),
                        // maintainState: false,
                        fullscreenDialog: true, //类似于 replace 跳转后的页面没有返回按钮, 只有关闭 fullscreenDialog
                        settings: RouteSettings(
                          arguments: 'data',
                        )
                    ),
                  );
                }
            ),
          ]
        ),
        // body: buildListView(),
        body: SmartRefresher(
          enablePullDown: true,
          enablePullUp: true,
          header: WaterDropHeader(),
          footer: ClassicFooter(
            loadStyle: LoadStyle.ShowAlways,
            completeDuration: Duration(microseconds: 50),
          ),
          onRefresh: _onRefresh,
          onLoading: _onLoading,
          controller: _controller,
          child: ListView.builder(itemBuilder: _itemBuilder,itemCount: choices.length),


        )
      ),
    );
  }

  ListView buildListView() {
    return ListView.separated(
      itemBuilder: (BuildContext context,int index){
        String netImageUrl = _imgList[index];
        return Padding(
          padding: EdgeInsets.all(10.0),
          child: FadeInImage.memoryNetwork(
            placeholder: kTransparentImage,
            image: netImageUrl,
          ),
        );
      },
      itemCount: _imgList.length,
      ///构建每个子Item之间的间隔Widget
      separatorBuilder: (BuildContext context, int index) {
        return new Container();
      },
      physics: const AlwaysScrollableScrollPhysics()
    );
  }

  // Future<Null> _refresh() async{
  //   print("tab 下拉刷新");
  // }

  // TabBarView buildTabView() {
  //   return TabBarView(
  //     children: choices.map((Choice choice) {
  //       return RefreshIndicator(
  //         child: buildListView(),
  //         // 'The onRefresh callback returned null.\n'
  //         // 'The RefreshIndicator onRefresh callback must return a Future.'
  //         onRefresh: _refresh,
  //       );
  //     }).toList(),
  //   );
  // }

  // StaggeredGridView buildListView() {
  //   return StaggeredGridView.countBuilder(
  //     crossAxisCount: 4,
  //     itemCount: _imgList.length,
  //     itemBuilder: (BuildContext context,int index){
  //       String netImageUrl = _imgList[index];
  //       return FadeInImage.memoryNetwork(
  //         placeholder: kTransparentImage,
  //         image: netImageUrl,
  //       );
  //     },
  //     staggeredTileBuilder: (int index) =>
  //     new StaggeredTile.count(2, index.isEven ? 2 : 1),
  //     mainAxisSpacing: 4.0,
  //     crossAxisSpacing: 4.0,
  //   );
  // }

  @override
  bool get wantKeepAlive => true;

  void initState() {
    super.initState();

    Future.delayed(Duration.zero, () {
      _getImgList();
    });
  }

  void _getImgList() {
    Http.request('http://shibe.online/api/shibes?count=1000&urls=true&httpsUrls=true').then((res) {
      setState(() {
        print(res);
        _imgList = res;
      });
    });
  }

}
