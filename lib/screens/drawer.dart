import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
// import './setting.dart';
import '../widget/WebviewWidget.dart';

class DrawerHead extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer( // 重要的Drawer组件
      elevation: 0,
      child: ListView( // Flutter 可滚动组件
        padding: EdgeInsets.zero, // padding为0
        children: <Widget>[
          UserAccountsDrawerHeader(
            // UserAccountsDrawerHeader 可以设置用户头像、用户名、Email 等信息，显示一个符合纸墨设计规范的 drawer header。
            // 标题
            accountName: Text('Jobsofferings',
                style: TextStyle(fontWeight: FontWeight.bold)),
            // 副标题
            accountEmail: Text('https://juejin.im/user/3350967174571143'),
            // Emails
            currentAccountPicture: CircleAvatar(
              // 使用网络加载图像
              backgroundImage: NetworkImage(
                  'https://images.cnblogs.com/cnblogs_com/JobsOfferings/1363202/o_preview.jpg'),
            ),
            // 圆角头像
            decoration: BoxDecoration(
                // color: Colors.yellow[400],
                image: DecorationImage(
                    image: NetworkImage(
                        'http://pic.netbian.com/uploads/allimg/190510/221228-15574975489aa1.jpg'),
                    // 控制图片填充效果
                    fit: BoxFit.cover, // 一种图像的布局方式
                    colorFilter: ColorFilter.mode(
                        Colors.grey[400].withOpacity(0.6),
                        BlendMode.hardLight))),
            //  BoxDecoration 用于制作背景
          ),
          // ListTile是下方的几个可点按List
          ListTile(
            // List标题
            title: Text('消息'),
            trailing: Icon(
              Icons.message_outlined, // Icon种类
              color: Colors.blue, // Icon颜色
              size: 22.0, // Icon大小
            ),
            // 点按时间，这里可以做你想做的事情，如跳转、判断等等
            // 此处博主只使用了 Navigator.pop(context) 来手动关闭Drawer
            onTap: () => Navigator.pop(context),
          ),
          ListTile(
            title: Text('收藏'),
            trailing: Icon(
              Icons.favorite,
              // color: Theme.of(context).accentColor,
              size: 22.0,
              color: Colors.blue,
            ),
            onTap: () => Navigator.pop(context),
          ),
          ListTile(
            title: Text('设置'),
            trailing: Icon(
              Icons.settings,
              color: Colors.blue,
              size: 22.0,
            ),
            onTap: () =>
              Navigator.pushNamed(context, '/setting', arguments: '路由参数'),
              // Navigator.of(context).push(
              //   MaterialPageRoute(
              //     builder: (context) => SettingScreen(),
              //     // maintainState: false,
              //     fullscreenDialog: false, //类似于 replace 跳转后的页面没有返回按钮, 只有关闭 fullscreenDialog
              //     settings: RouteSettings(
              //       arguments: 'data',
              //     )
              //   ),
              // )
          ),
          ListTile(
            title: Text('Flutter 中文社区'),
            trailing: Icon(
              Icons.local_mall_outlined,
              color: Colors.blue,
            ),
            onTap: () => {
            Navigator.of(context)
                .push(new MaterialPageRoute(builder: (_) {
                  return Browser(
                  url: "https://flutter-io.cn/",
                  title: "Flutter 中文社区",
                );
            }))
            },
          ),
          ListTile(
            title: Text('基本功能'),
            trailing: Icon(
              Icons.functions,
              color: Colors.blue,
            ),
            onTap: () {
              Navigator.pushNamed(context, '/functions/list', arguments: '路由参数');
            },
          )
        ],
      ),
    );
  }
}
