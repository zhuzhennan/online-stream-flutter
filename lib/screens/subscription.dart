import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../base.dart';

class SubscriptionScreen extends StatefulWidget {
  @override
  _SubscriptionScreenState createState() => _SubscriptionScreenState();
}

class _SubscriptionScreenState extends State<SubscriptionScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("订阅是"),
      ),
      body: Column(
        children: [
          Center(
            child: Text('订阅'),
          ),
          Container(
            color: Theme.of(context).secondaryHeaderColor,
            child: Text(
            'Text with a background color',
             style: Theme.of(context).textTheme.headline6,
            ),
          ),
          GestureDetector(
            onTap: () {

            },
            child: Text(0 == 0 ? DYBase.baseUrl : '快速注册',
              style: TextStyle(
                color: Color(0xffff7701),
                decorationStyle: TextDecorationStyle.solid,
                decorationColor: Color(0xffff7701),
                decoration: TextDecoration.underline,
              ),
            ),
          )
        ],
      ),
    );
  }
}

