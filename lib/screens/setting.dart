import 'package:flutter/material.dart';

class SettingScreen extends StatefulWidget {
  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  @override
  Widget build(BuildContext context) {
    String str = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text('设置'),
      ),
      body: ListView(
        children: [
          ListTile(
            // List标题
            title: Text(str),
            trailing: Icon(
              Icons.arrow_forward_ios_rounded, // Icon种类
              color: Colors.black12, // Icon颜色
              size: 22.0, // Icon大小
            ),
            // 点按时间，这里可以做你想做的事情，如跳转、判断等等
            // 此处博主只使用了 Navigator.pop(context) 来手动关闭Drawer
            onTap: () => Navigator.pop(context),
          ),
        ],
      ),
    );
  }

  // initState didChangeDependencies build
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print(ModalRoute.of(context).settings.arguments);
  }
}
