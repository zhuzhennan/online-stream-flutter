import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../widget/common/ConfirmDialog.dart';

class Functions {
  final String method;
  final Icon icon;

  Functions(this.method, this.icon);
}

class BaseFunctions extends StatefulWidget {
  @override
  _BaseFunctionsState createState() => _BaseFunctionsState();
}

class _BaseFunctionsState extends State<BaseFunctions> {
  final List<Functions> _functions = [Functions('下拉刷新', Icon(Icons.arrow_forward_ios_outlined))];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('基本功能')
      ),
      body: ListView.separated(itemBuilder: (BuildContext context, int index) {
        Functions item = _functions[index];
          return ListTile(
            title: Text(item.method),
            trailing: item.icon,
            onTap: () {
              showDialog(context: context, builder: (content) {
                return AlertDialog(
                  title: Text("title"),
                  content: Text("body"),
                  actions: <Widget>[
                    FlatButton(onPressed: () {
                      Navigator.of(context).pop();
                    },
                        child: Text("confirmText")
                    ),
                    FlatButton(onPressed: () {
                      Navigator.of(context).pop();
                    },
                        child: Text("cancelText"))
                  ],
                );
              });
              // Fluttertoast.showToast(msg: item.method, gravity: ToastGravity.CENTER);
            },
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          return Divider(color: Colors.blue,);
        },
        itemCount: _functions.length),
      );
  }
}
