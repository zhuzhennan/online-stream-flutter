import 'package:flutter/material.dart';

class ShowConfirmDialog extends StatelessWidget {
  final String title;
  final String body;
  final String confirmText;
  final String cancelText;
  final Function confirmCallback;

  ShowConfirmDialog({
      @required this.body,
      this.title = '提示',
      this.confirmText = "确认",
      this.confirmCallback,
      this.cancelText = "取消"
  });

  @override
  Widget build(BuildContext context) {
    showDialog(context: context, builder: (content) {
      return AlertDialog(
        title: Text(title),
        content: Text(body),
        actions: <Widget>[
          FlatButton(onPressed: () {
            confirmCallback();
            Navigator.of(context).pop();
          },
              child: Text(confirmText)
          ),
          FlatButton(onPressed: () {
            Navigator.of(context).pop();
          },
              child: Text(cancelText))
        ],
      );
    });
  }
}
