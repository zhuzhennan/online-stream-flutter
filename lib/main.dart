import 'package:flutter/material.dart';
import 'package:jpush_flutter/jpush_flutter.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:io';

import './screens/home.dart';
import './screens/entertainment.dart';
import './screens/find.dart';
import './screens/profile.dart';
import './screens/subscription.dart';
import './screens/drawer.dart';
import './screens/setting.dart';
import './widget/BaseWidgets.dart';
import './screens/viewHistory.dart';

void main() {
  runApp(MyApp());

  // 透明状态栏
  if (Platform.isAndroid) {
    SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(statusBarColor: Colors.transparent);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          scaffoldBackgroundColor: Color.fromARGB(255, 255, 255, 255),
          primarySwatch: Colors.orange,
          // ignore: deprecated_member_use
          textTheme: TextTheme(body1: TextStyle(color: Colors.black)),
          appBarTheme: AppBarTheme(
            textTheme: TextTheme(
              title: TextStyle(
                color: Colors.black,
                fontSize: 18,
              ),
              // ignore: deprecated_member_use
              body1: TextStyle(color: Colors.black),
            ),
          )
        // splashFactory: NoSplashFactory()
      ),
      home: MyHomePage(title: 'Flutter'),
      routes: <String, WidgetBuilder>{
        '/setting': (BuildContext context) => SettingScreen(),
        '/functions/list': (BuildContext context) => BaseFunctions(),
        '/view-history': (BuildContext content) => ViewHistory()
      },

    );
  }

}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentIndex = 0;
  DateTime _lastCloseApp; //上次点击返回按钮时间

  final List<Widget> pages = [
    HomeScreen(),
    EntertainmentScreen(),
    SubscriptionScreen(),
    FindScreen(),
    ProfileScreen()
  ];

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return WillPopScope(
      onWillPop: () async {
        if (_lastCloseApp == null || DateTime.now().difference(_lastCloseApp) > Duration(seconds: 1)) {
          _lastCloseApp = DateTime.now();
          Fluttertoast.showToast(msg: '再次返回退出斗鱼');
          return false;
        }
        return true;
      },
      child: Scaffold(
          backgroundColor: Colors.white,
          body: IndexedStack(
            index: _currentIndex,
            children: pages,
          ),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            items: [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: '首页'),
              BottomNavigationBarItem(icon: Icon(Icons.star_rate_rounded), label: '娱乐'),
              BottomNavigationBarItem(icon: Icon(Icons.favorite), label: '订阅'),
              BottomNavigationBarItem(icon: Icon(Icons.filter_tilt_shift), label: '发现'),
              BottomNavigationBarItem(icon: Icon(Icons.insert_emoticon), label: '我的'),
            ],
            currentIndex: _currentIndex,
            onTap: (index) {
              if (index != _currentIndex) {
                setState(() {
                  _currentIndex = index;
                });
              }
            },
          ),
          drawer: DrawerHead()
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    JPush jpush = new JPush();
    jpush.setup(
      appKey: "122abf259c4a974bf2aa697d",
      channel: "flutter_channel",
      production: false,
      debug: true,      //是否打印debug日志
    );
  }
}
