import 'package:flutter/material.dart';
import '../screens/home.dart';
import '../screens/entertainment.dart';
import '../screens/find.dart';
import '../screens/profile.dart';
import '../screens/subscription.dart';
import '../screens/drawer.dart';
import '../screens/setting.dart';

class Config {
   Map<String, WidgetBuilder> routes = {
     '/setting': (BuildContext context) => new SettingScreen(),
   };

   Config(this.routes);
}